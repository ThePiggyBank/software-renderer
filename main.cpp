#include "image.hpp"
#include <iostream>

class Shape
{
public:
    virtual void draw(Image& img) = 0;
};

class Circle : public Shape
{
public:
    Circle(unsigned x_, unsigned y_, unsigned r_, Colour col_)
    {
        x = x_;
        y = y_;
        r = r_;
        col = col_; 
    }

    void draw(Image& img) override;
    
    unsigned x;
    unsigned y;
    unsigned r;
    Colour col;
};



void Circle::draw(Image& img)
{
    unsigned xFrom = 0, xTo = img.width()-1;
    unsigned yFrom = 0, yTo = img.height()-1;

    if (x >= r) xFrom = x-r;
    if (y >= r) yFrom = y-r;
    xTo = std::min(x+r, xTo);
    yTo = std::min(y+r, yTo);

    for (unsigned i = xFrom; i <= xTo; ++i)
    {
        for (unsigned j = yFrom; j <= yTo; ++j)
        {
            int dx = (int)x-(int)i;
            int dy = (int)y-(int)j;
            if (dx*dx + dy*dy <= r*r)
            {
                img.setPixel(i, j, col);
            }
        }
    }
}

class Rectangle : public Shape
{
public:
    Rectangle(unsigned x0_, unsigned y0_, unsigned x1_, unsigned y1_, Colour col_)
    {
        x0 = x0_;
        x1 = x1_;
        y0 = y0_;
        y1 = y1_;
        col = col_;
    }
    
    void draw(Image& img) override;
        
    unsigned x0;
    unsigned x1;
    unsigned y0;
    unsigned y1;
    Colour col;
};

void Rectangle::draw(Image& img)
{
    if (x0 >= img.width() || y0 >= img.height()) return;
    for (unsigned x = x0; x <= std::min(x1, img.width() -1); ++x)
    {
        for (unsigned y = y0; y <= std::min(y1, img.height() -1); ++y)
        {
            img.setPixel(x, y, col); 
        }
    }
}

void drawShape(Shape& shape, Image& img)
{
    shape.draw(img);
}

int main()
{
    std::cout << "> " << std::flush;
    unsigned width;
    unsigned height;
    std::cin >> width >> height;

    Image img;
    img.resize(width, height);
    
    
    for (int circleAmount = 0; circleAmount < 1000; ++circleAmount)
    {
        Colour col = {0, 0, 0};
        if (circleAmount % 3 == 0) col.r = circleAmount % 128 + 128;
        
        if (circleAmount % 3 == 1) col.g = circleAmount % 128 + 128;
        
        if (circleAmount % 3 == 2) col.b = circleAmount % 128 + 128;
        
        unsigned distance = 10;
        unsigned r = circleAmount * 17 % 44 + 7;
        
        Circle circ(circleAmount * circleAmount * 2971 % width,
                    circleAmount * circleAmount * 5483 % height, r, col);
        //circ.draw(img);
        drawShape(circ, img);  
    }
    
    Rectangle rect(50, 50, 200, 200, {255, 255, 255});
    //rect.draw(img);
    drawShape(rect, img);
    writeImage(img, "output/picture.pnm");
}
