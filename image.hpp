#include <string>
#include <vector>

using Byte = unsigned char;

struct Colour
{
    Byte r;
    Byte g;
    Byte b;
};

class Image
{
public:
    void setPixel(unsigned x, unsigned y, Colour col);
    Colour getPixel(unsigned x, unsigned y) const;
    void resize(unsigned resizedWidth, unsigned resizedHeight);
    unsigned width() const;
    unsigned height() const;
    
private:
    friend void writeImage(const Image& img, std::string filename);

    unsigned getIndex(unsigned x, unsigned y) const;
    std::vector<Byte> m_data;
    unsigned m_width = 0;
    unsigned m_height = 0;
};

void writeImage(const Image& img, std::string filename);

